migrate:
	docker-compose exec web python manage.py makemigrations
	docker-compose exec web python manage.py migrate

up:
	docker-compose up -d


down:
	docker-compose down